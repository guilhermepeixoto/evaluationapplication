import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Category } from '../../../models';
import { RequestsService } from '../../../services';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss'],
  providers: [RequestsService]
})
export class EditCategoryComponent implements OnInit {

  public category: Category;

  public failMgs: string = '';

  constructor(
    private requests: RequestsService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    let category_id = this.activeRoute.snapshot.params;

    this.getCategoryById(category_id.id);
  }

  //Recupera o documento para ser editado pelo id
  getCategoryById(id: string): void {
    this.requests.getById('categoria', id).then(res => {
      this.category = res;
    });
  }

  updateCategory(): void {
    this.requests.update('categoria', this.category).then(res => {
      if (res) {
        alert("Categoria atualizada com sucesso!");
        this.router.navigate(['/categoria']);
      }
      else {
        this.failMgs = 'Falha ao tentar remover a categoria.';
      }

      setTimeout(() => {
        this.failMgs = '';
      }, 6000);
    });
  }
}
