import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListCategoryComponent } from './list-category';
import { NewCategoryComponent } from './new-category';
import { EditCategoryComponent } from './edit-category';

export const CategoryRoutes: Routes = [
    { path: '', component: ListCategoryComponent },
    { path: '', children: [
        { path: 'novo', component: NewCategoryComponent },
        { path: 'editar/:id', component: EditCategoryComponent }
    ]}
]
