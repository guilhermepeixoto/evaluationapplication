import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { CategoryRoutes } from './category.routing';
import { ListCategoryComponent } from './list-category/list-category.component';
import { NewCategoryComponent } from './new-category/new-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CategoryRoutes),
    FormsModule,
    MDBBootstrapModule
  ],
  declarations: [ListCategoryComponent, NewCategoryComponent, EditCategoryComponent]
})
export class CategoryModule { }
