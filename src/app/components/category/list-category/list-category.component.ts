import { Component, OnInit } from '@angular/core';

import { RequestsService } from './../../../services';
import { Category } from './../../../models';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
  providers: [RequestsService]
})
export class ListCategoryComponent implements OnInit {

  public categories: Array<Category> = [];
  public successMgs: string = '';
  public failMgs: string = '';

  constructor(
    private requests: RequestsService
  ) { }

  ngOnInit() {
    this.getAllCategories();
  }

  //Buscar todas categorias
  getAllCategories(): void {
    this.requests.getAll('categoria').then(res => {
      this.categories = res;
    });
  }

  //Remover categoria
  removeCategory(category: Category): void {
    if (confirm(`Deseja realmente remover a categoria ${category.name}`)) {
      this.requests.remove('categoria', category._id).then(res => {
        if(res) {
          this.successMgs = 'Categoria removida com sucesso.';
          this.getAllCategories();              
        }
        else {
          this.failMgs = 'Falha ao tentar remover a categoria.';
        }

        setTimeout(() => {
          this.successMgs = '';
          this.failMgs = '';
        }, 6000);
      })
    }
  }
}
