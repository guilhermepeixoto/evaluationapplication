import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Category } from './../../../models';
import { RequestsService } from './../../../services';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.scss'],
  providers: [RequestsService]
})
export class NewCategoryComponent implements OnInit {

  public category: Category;

  public failMgs: string = '';

  constructor(
    private requests: RequestsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.category = new Category();
  }

  createCategory(): void {
    this.requests.create('categoria', this.category).then(res => {
      if(res) {
        alert("Categoria criada com sucesso!");
        this.router.navigate(['/categoria']);            
      }
      else {
        this.failMgs = 'Falha ao tentar remover a categoria.';
      }

      setTimeout(() => {
        this.failMgs = '';
      }, 6000);
    });
  }
}
