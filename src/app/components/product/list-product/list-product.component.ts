import { Component, OnInit } from '@angular/core';

import { RequestsService } from './../../../services';
import { Product } from './../../../models';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss'],
  providers: [RequestsService]
})
export class ListProductComponent implements OnInit {

  public products: Array<Product> = [];
  public successMgs: string = '';
  public failMgs: string = '';

  constructor(
    private requests: RequestsService
  ) { }

  ngOnInit() {
    this.getAllProducts();
  }

  //Buscar todas categorias
  getAllProducts(): void {
    this.requests.getAll('produto').then(res => {
      this.products = res;
    })
  }

  //Remover categoria
  removeProduct(products: Product): void {
    if (confirm(`Deseja realmente remover a produto ${products.name}`)) {
      this.requests.remove('produto', products._id).then(res => {
        if (res) {
          this.successMgs = 'Produto removido com sucesso.';
          this.getAllProducts();
        }
        else {
          this.failMgs = 'Falha ao tentar remover a produto.';
        }

        setTimeout(() => {
          this.successMgs = '';
          this.failMgs = '';
        }, 6000);
      })
    }
  }
}
