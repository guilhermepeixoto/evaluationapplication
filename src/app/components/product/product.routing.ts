import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListProductComponent } from './list-product';
import { NewProductComponent } from './new-product';
import { EditProductComponent } from './edit-product';

export const ProductRoutes: Routes = [
    { path: '', component: ListProductComponent },
    { path: '', children: [
        { path: 'novo', component: NewProductComponent },
        { path: 'editar/:id', component: EditProductComponent }
    ]}
]
