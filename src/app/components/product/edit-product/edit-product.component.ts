import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Product, Category } from '../../../models';
import { RequestsService } from '../../../services';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
  providers: [RequestsService]
})
export class EditProductComponent implements OnInit {

  public product: Product;
  public categories: Array<Category> = [];
  public failMgs: string = '';

  constructor(
    private requests: RequestsService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    let category_id = this.activeRoute.snapshot.params;

    this.getProductById(category_id.id);
    this.getCategories();
  }

  //Dependencia para criação de produto - Categoria  
  getCategories(): void {
    this.requests.getAll('categoria').then(res => {
      this.categories = res;
    });
  }

  //Recupera o documento para ser editado pelo id
  getProductById(id: string): void {
    this.requests.getById('produto', id).then(res => {
      this.product = res;
    });
  }

  updateProduct(): void {
    this.requests.update('produto', this.product).then(res => {
      if (res) {
        alert("Produto atualizado com sucesso!");
        this.router.navigate(['/produto']);
      }
      else {
        this.failMgs = 'Falha ao tentar remover o produto.';
      }

      setTimeout(() => {
        this.failMgs = '';
      }, 6000);
    });
  }

}
