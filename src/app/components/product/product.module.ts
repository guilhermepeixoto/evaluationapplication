import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { ProductRoutes } from './product.routing';

import { ListProductComponent } from './list-product/list-product.component';
import { NewProductComponent } from './new-product/new-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MDBBootstrapModule,
    RouterModule.forChild(ProductRoutes),
    CurrencyMaskModule
  ],
  declarations: [ListProductComponent, NewProductComponent, EditProductComponent]
})
export class ProductModule { }
