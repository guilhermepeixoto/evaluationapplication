import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product, Category } from './../../../models';
import { RequestsService } from './../../../services';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss'],
  providers: [RequestsService]
})
export class NewProductComponent implements OnInit {

  public product: Product;
  public categories: Array<Category> = [];

  public failMgs: string = '';

  constructor(
    private requests: RequestsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.product = new Product();

    this.getCategories();
  }

  //Dependencia para criação de produto - Categoria
  getCategories(): void {
    this.requests.getAll('categoria').then(res => {
      this.categories = res;
    });
  }

  createProduct(): void {
    this.requests.create('produto', this.product).then(res => {
      if (res) {
        alert("Produto criado com sucesso!");
        this.router.navigate(['/produto']);
      }
      else {
        this.failMgs = 'Falha ao tentar remover a produto.';
      }

      setTimeout(() => {
        this.failMgs = '';
      }, 6000);
    });
  }
}
