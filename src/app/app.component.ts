import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  public loading: boolean = true;

  ngOnInit() {
    let sec = Math.random() * (3000 - 1600) + 1600;
    
    setTimeout(() => {
      this.loading = false;
    }, sec);
  }
}
