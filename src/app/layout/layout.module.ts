import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { HeaderComponent } from './header/header.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  imports: [
    CommonModule,
    MDBBootstrapModule,
    RouterModule
  ],
  declarations: [HeaderComponent, LoaderComponent],
  exports: [HeaderComponent, LoaderComponent]
})
export class LayoutModule { }
