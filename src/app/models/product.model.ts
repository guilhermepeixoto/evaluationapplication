import { Category } from ".";


export class Product {

    constructor(
        public _id?: string,
        public name?: string,
        public description?: string,
        public price?: number,
        public status?: boolean,
        public category?: Category,
        public createdOn?: any,
        public updatedOn?: any
    ) {}
}