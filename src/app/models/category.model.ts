export class Category {

    constructor(
        public _id?: string,
        public name?: string,
        public description?: string,
        public status?: boolean,
        public createdOn?: any,
        public updatedOn?: any
    ) {}
}