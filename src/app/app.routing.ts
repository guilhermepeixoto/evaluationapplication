import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home';

export const AppRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'categoria', children: [
        { path: '', loadChildren: './components/category/category.module#CategoryModule' }
    ]},
    { path: 'produto', children: [
        { path: '', loadChildren: './components/product/product.module#ProductModule' }
    ]}
]
