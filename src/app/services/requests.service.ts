import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class RequestsService {

  private readonly PORT = '62214';
  private readonly OPTIONS = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

  constructor(
    private http: Http
  ) { }

  /**
   * Retorna um array de coleção
   * 
   * @param controller 
   * @returns Promise<Array<Object>>
   */
  getAll(controller: string): Promise<Array<Object>> {
    return this.http.get(`http://localhost:${this.PORT}/api/${controller}`, this.OPTIONS)
      .toPromise()
      .then(res => {
        return res.json();
      }).catch(error => {
        return error;
      });
  }

  /**
   * Retorna uma coleção, passado o id como referência
   * 
   * @param controller 
   * @param id 
   * @returns Promise<Object>
   */
  getById(controller: string, id: string): Promise<Object> {
    return this.http.get(`http://localhost:${this.PORT}/api/${controller}/${id}`, this.OPTIONS)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(error => {
        return error;
      });
  }

  /**
   * Cria o documento
   * 
   * @param controller 
   * @param objBody 
   * @returns Promise<boolean>
   */
  create(controller: string, objBody: Object): Promise<boolean> {
    return this.http.post(`http://localhost:${this.PORT}/api/${controller}`, objBody, this.OPTIONS)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(error => {
        return error;
      });
  }

  /**
   * Atualiza o documento conforme id e retorna atualizado
   * @param controller 
   * @param objBodyUpdate 
   * @param id 
   * @returns Promise<Object>
   */
  update(controller: string, objBodyUpdate: Object) {    
    return this.http.put(`http://localhost:${this.PORT}/api/${controller}/${objBodyUpdate['_id']}`, objBodyUpdate, this.OPTIONS)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(error => {
        return error;
      });
  }

  /**
   * Remove um documento, passando o id como referência
   * 
   * @param controller 
   * @param id 
   * @returns Promise<boolean>
   */
  remove(controller: string, id: string): Promise<boolean> {
    return this.http.delete(`http://localhost:${this.PORT}/api/${controller}/${id}`, this.OPTIONS)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(error => {
        return error;
      });
  }
}
